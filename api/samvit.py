from config import *
import random
import sys,os
from db.mydb import *
from pprint import pprint
from resources.collections import *

class WordsDB(CollectionRsrc):
    def __init__(self):
        self.cname = 'words'
        self.key = ['word']
        self.schema = {
            'word': '',
            'type': '',
            'subtype': '',
            'linga': '',
            'padi': '',
            'meaning': '',
            'context': '',
            'subcontext': '',
            'audio_url': '',
            'image_url': '',
            'forms' : ''
        }
        CollectionRsrc.__init__(self, samvit().db()[self.cname])

class TemplatesDB(CollectionRsrc):
    def __init__(self):
        self.cname = 'templates'
        self.key = ['name']
        self.schema = {
            'name': '',
            'samvit_levelid' : '',
            'parms': [],
            'vaakya' : '',
            'components': []
        }
        CollectionRsrc.__init__(self, samvit().db()[self.cname])

class WordClustersDB(CollectionRsrc):
    def __init__(self):
        self.cname = 'word_clusters'
        #self.key = ['name']
        self.schema = {
            'cluster': '',
            'encoding':'',
            'samvit_levelid' : '',
            'karaka': '',
            'source': '' 
        }
        CollectionRsrc.__init__(self, samvit().db()[self.cname])

class SamvitKosha:
    name = ''
    collection = None
    def __init__(self, db, name):
        load = False
        self.name = name
        if name not in db.c:
            load = True
        self.collection = db[name]
        if load:
            self.load(cmdpath(name + ".csv"))

    def load(self, csvfile):
        if os.path.isfile(csvfile):
            print "Loading {} bank from {}".format(self.name, csvfile)
            self.collection.fromCSV(csvfile)

    def get(self, query):
        return self.collection.find(query)

    def sorted(self, query):
        return self.collection.find(query).sort()

    def unique(self, query):
        return self.collection.distinct('word', query).sort()

    def rand(self, query):
        return self.collection.find(query)

class _Samvit:
    _db = None
    def __init__(self, reset=False):
        self._db = MyDB(serverconfig()['samvit_db'])
        if reset:
            self.db().reset()
        SamvitKosha(self.db(), 'words')
        SamvitKosha(self.db(), 'templates')
        SamvitKosha(self.db(), 'word_clusters')

    def db(self):
        return self._db

_samvit = None
def samvit():
    global _samvit
    return _samvit

def samvit_init(reset=False):
    global _samvit
    _samvit = _Samvit(reset)
